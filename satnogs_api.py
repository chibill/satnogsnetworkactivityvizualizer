import requests


NetworkJobsAPI = "https://network.satnogs.org/api/jobs/"
NetworkStationAPI = "https://network.satnogs.org/api/stations/" #Paeginated


DBSatelliteAPI  = "https://db.satnogs.org/api/satellites/"
DBTransmitterAPI = "https://db.satnogs.org/api/transmitters/"



def getStringForStatus(status): 
    "Takes in a status number and returns a string"
    if status == 0:
        return "Offline"
    if status == 1:
        return "Testing"
    if status == 2:
        return "Online"
    return "Error Status no found"


def getColorForStatus(status): 
    "Takes in a status number and returns a tuple of 4 ints between 0 and 255 as an RGBA color"
    if status == 0:
        return (255, 0, 0, 50)
    if status == 1:
        return (248, 148, 6, 255
    if status == 2:
        return (0, 230, 64, 255)
    return (0,0,0,0) #Not found

def getStations(): 
    """
        This function returns a dict of stations with their ID as the key.
        
        A station is a dict with the following required keys
        
        id: The station's ID
        lat: The station's Latitude
        lng: The station's longitutde
        alt: The station's Altitude
        status: The station's status number. Used to color the stations in the visualization and also group them for displaying.  

        The following optional keys exist
        name: Name of the station - Used for when clicking on a station if missing only the ID will be displayed.
        description: Discription of the station to show when you click on it. If missing no description will be displayed.
    """
    stations = {} 
    
    last_request = requests.get(NetworkStationAPI)
    while last_request.status_code != 404:
        for station in last_request.json():
            s_id = station["id"]
            stations[s_id] = {"id":s_id}
            stations[s_id]["name"] = station["name"]
            stations[s_id]["lat"] = station["lat"]
            stations[s_id]["lng"] = station["lng"]
            stations[s_id]["alt"] = station["altitude"]
            status = 0
            if station["status"] == "Testing":
                status = 1
            elif station["status"] == "Online":
                status = 2
            stations[s_id]["status"] = status
            stations[s_id]["destiption"] = station["description"]

        last_request = request.get(NetworkStationAPI)
    return stations            
    

def getObjects():
    """
        This fucntion returns all the objects that will be ever be displayed it returns a dict of objects with their ID as the key.

        The dist has the following required keys
            
        id: Object's ID
        
        Optional Keys are the following
        
        name: The objects name
        description: The objects description
        
    """

    satellites = {}
    
    sats = requests.get(DBSatelliteAPI).json()

    for x in sats:
        satellites[x["sat_id"]] = {"id":x["sat_id"],"name",x["name"]}

    return satellites
            


def getObservations():
    """
       This function return a lsit of all the observations that will be being ploted on the vizualizer. It will be called at a configurable rate to get new data to process.
        
        An observation has the following required keys

        object_id: the id of the Object being observer
        station_id: the id of the station doing the observing.
        start: timestamp (datetime object) of when the observation starts
        end: timestamp (datetime object) of when the observation ends)
        tle: the TWO line elements for the observation
        
    """
    pass
